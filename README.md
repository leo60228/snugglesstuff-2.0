# README #

### What is this repository for? ###

This repo is for the source code of the SnugglesStuff 2.0 project.

### How do I get set up? ###

1. **Install** node.js.
2. In a terminal, **run** `npm install -g grunt-cli`.
3. **Get** a copy of this repo by going to Downloads, clicking "Download repository", then extracting the zip file.
4. In a terminal in the folder you extracted, **run** `npm install`.
5. Now **run** `grunt`.
6. To start the web server, **run** `node app.js 8080`. Replace 8080 with the port to run the server on.

### Contribution guidelines ###

* Be nice.
* Use tabs for spacing in CSS and HTML to keep things consistent.
* Use dual-spaces for spacing in JS to keep things consistent.

### Who do I talk to? ###

snuggles08. PM him on SnugglesStuff 1.0.