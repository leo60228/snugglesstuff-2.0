$(document).ready(function(){
	resizeDiv();
});

window.onresize = function(event) {
	resizeDiv();
}

function resizeDiv() {
	vpw = $(window).width();
	vph = $(window).height();
	$('.bgmain').css({'height': vph + 'px'});
}

function nextSlide() {
	//if (typeof $ == "undefined") var $ = function(sel) { return document.querySelector(sel); }
	if (!$(".slides img:first-child").is(":visible")) {
		$(".slides img:first-child").show();
	}
	$(".slides img:last-child").fadeOut(1000, function() {
		$(".slides").append($(".slides img:first-child"));
	});
}

setInterval(nextSlide, 3000);
