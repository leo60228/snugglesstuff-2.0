var fs = require('fs');

var express = require('express');
var serveIndex = require('serve-index');
var app = express();

var host = '127.0.0.1';
var port = process.env.PORT || process.argv[2] || 8080;

app.engine('ejs', require('ejs').__express);

app.set('views', __dirname + '/public');

app.get('*.ejs', function(req, res, next) {
	var file = '.' + req.params[0] + '.ejs';
	if (fs.existsSync('public/' + file)) {
		res.render(file);
	} else {
			}
	next();
});


app.use(express.static(__dirname + '/public'));

app.get('*', function(req, res, next) {
	var file = '.' + req.params[0] + 'index.ejs';
	if (fs.existsSync('public/' + file)) {
		res.render(file);
	} else {
		next();
	}
	//console.log(file);
	//next();
});


app.use(serveIndex(__dirname + '/public'));

app.get('*', function(req, res, next) {
	res.status(404).send('<p><h1>404</h1></p><p>File not found!</p>');
});

app.listen(port);

console.log('Example app listening at http://%s:%s', host, port);
