/*global module:false*/
module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);
	
	// Project configuration.
	grunt.initConfig({
		uglify: {
			all: {
				files: [{
					expand: true,
					cwd: 'js/',
					src: [
						'*.js',
						'**/*.js'
					],
					dest: 'public/build/js/',
					ext: '.min.js',
				}],
			}
		},

		sass: {
			options: {
				sourceMap: false,
				outputStyle: 'compressed'
			},
			dist: {
				files: [
						{
							expand: true,
							cwd: 'style/',
							src: ['**/*.scss'],
							dest: 'public/build/css',
							ext: '.css',
							extDot: 'first'
						}
				]
			}
		},

		watch: {
			js: {
				files: ['js/*.js', 'js/**/*.js'],
				tasks: ['uglify'],
			},
			sass: {
				files: ['style/*.scss', 'style/**/*.scss'],
				tasks: ['sass'],
			}
		},

		mkdir: {
			all: {
				options: {
					create: ['public/build/css', 'public/build/js']
				}
			}
		},

		clean: ['public/build']
	});

	// Default task.
	grunt.registerTask(
		'default', [
			'clean',
			'mkdir',
			'uglify',
			'sass'
		]);

	
	// Heroku task
	grunt.registerTask(
		'heroku', [
			'clean',
			'mkdir',
			'uglify',
			'sass'
		]);
	

};
